# GitHub API specifications and testing

## The goal

Write demo automated tests for the GitHub API.

- Automate the [OAuth Non-Web authentication flow](https://developer.github.com/v3/oauth/#non-web-application-flow)
- Pick one or two API endpoints and show how you would validate its functionality.

## Implementation

The Dredd utility is reading Blueprint API specification (or Swagger can be used instead) and is checking them on the fly.

## Testing result example

```
info: Configuration './dredd-local.yml' found, ignoring other arguments.
info: Starting backend server process with command: node app.js
info: Waiting 3 seconds for backend server process to start.
info: Beginning Dredd testing...
info: Found Hookfiles: 0=hooks/github-api.php
info: Spawning `vendor/bin/dredd-hooks-php` hooks handler process.
info: Hooks handler stdout: Starting server

info: Successfully connected to hooks handler. Waiting 0.1s to start testing.
pass: GET /users/technoweenie duration: NaNms
pass: GET /gists duration: NaNms
pass: POST /gists duration: NaNms
pass: GET /gists/d7b4325ac95ca49ef310aed14dfa8b15 duration: NaNms
pass: DELETE /gists/d7b4325ac95ca49ef310aed14dfa8b15 duration: NaNms
info: Sending SIGTERM to hooks handler process.
complete: 5 passing, 0 failing, 0 errors, 0 skipped, 5 total
complete: Tests took 3025ms
complete: See results in Apiary at: https://app.apiary.io/githubtest.../tests/run/4dbe-...-...9157
```

## Before start

- Go to the Apiary.io and [create new project](https://app.apiary.io/githubtest5/create-api),
  for example ```https://app.apiary.io/githubtest5/tests/runs#tutorial```
    - Go to the [Tutorial tab](https://app.apiary.io/githubtest5/tests/runs#tutorial)
    and find your ```apiaryApiKey``` and ```apiaryApiName```. Skip other instructions on the page.
- [Setup a new token](https://github.com/settings/tokens) for the test goals with scopes: *gist, user:email*.
Do not forget to copy new token before closing tab.
- Checkout this repo to some folder

## API Specifications example

- Look into [github-api.md](/github-api.md). You can copy it into your https://app.apiary.io/ Editor to play online.

## Run with Vagrant

### Requirements

- [Vagrant](https://www.vagrantup.com)

### Execution

- Put your Apiary parameters and GitHub token below and run the command:
```
vagrant box update;
DREDD_GITHUB_TEST_PATH=<path-to-dir-with-current-file> APIARY_API_KEY=<...> APIARY_API_NAME=<...> GITHUB_API_TOKEN=<...> vagrant up
```
- After first launch you can run ```vagrant ssh``` and ```cd /var/dredd_test && composer check```

## OR Install to localhost

### Requirements

- NodeJs and Npm.
- PHP 7.
- [Composer](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx): ```curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer```

### Installation steps

- Run: ```composer install && npm install```

### Execution on localhost

- Put your Apiary parameters and GitHub token below and run the command:
```
APIARY_API_KEY=<...> APIARY_API_NAME=<...> GITHUB_API_TOKEN=<...> composer check
```