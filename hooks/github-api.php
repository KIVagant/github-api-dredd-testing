<?php

use Dredd\Hooks;

const DEFAULT_GIST_ID = "a123456bcd";

$scope = [
    'lastGistId' => ''
];

Hooks::after("Gists Collection > Creating new Gist", function (&$transaction) use (&$scope) {
    $scope['lastGistId'] = '';
    if (!isset($transaction->real->body)) {
        $transaction->fail = true;

        return;
    }
    $body = json_decode($transaction->real->body, true);
    if (!isset($body['id'])) {
        $transaction->fail = true;

        return;
    }
    $scope['lastGistId'] = $body['id'];
});

Hooks::before("Gists Collection > Load a Gist", function (&$transaction) use (&$scope) {
    $transaction->expected->body = str_replace(DEFAULT_GIST_ID, $scope['lastGistId'], $transaction->expected->body);
    $transaction->id = str_replace(DEFAULT_GIST_ID, $scope['lastGistId'], $transaction->id);
    $transaction->request->uri = str_replace(DEFAULT_GIST_ID, $scope['lastGistId'], $transaction->request->uri);
    $transaction->fullPath = str_replace(DEFAULT_GIST_ID, $scope['lastGistId'], $transaction->fullPath);
});

Hooks::before("Gists Collection > Delete a Gist", function (&$transaction) use (&$scope) {
    $transaction->expected->body = str_replace(DEFAULT_GIST_ID, $scope['lastGistId'], $transaction->expected->body);
    $transaction->id = str_replace(DEFAULT_GIST_ID, $scope['lastGistId'], $transaction->id);
    $transaction->request->uri = str_replace(DEFAULT_GIST_ID, $scope['lastGistId'], $transaction->request->uri);
    $transaction->fullPath = str_replace(DEFAULT_GIST_ID, $scope['lastGistId'], $transaction->fullPath);
});

