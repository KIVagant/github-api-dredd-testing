#!/usr/bin/env bash

sudo /usr/bin/ssh-keygen -A
sudo echo "root:root" | chpasswd
#sudo groupadd www-data
#useradd -g www-data www-data
#sudo sed -i "s/SELINUX=[a-z]*/SELINUX=disabled/g" /etc/selinux/config #box will need to reload
yum -y clean all

sudo rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sudo rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
sudo curl --silent --location https://rpm.nodesource.com/setup_6.x | bash -
yum -y update

sudo yum -y install curl git nodejs npm php70w-cli --skip-broken \
&& curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/bin --filename=composer \
&& yum -y install gcc-c++ make \
&& sudo npm cache clean \
&& sudo npm install dredd -g \
&& sudo npm install express -g